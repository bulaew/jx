####Install dependencies####

```
#!javascript

npm install
```

####For build####

```
#!javascript

npm run build
```

####For dev####

```
#!javascript

npm run dev
```


### There are two builds jx-web and jx-node. ###
* jx-web.js includes all required dependencies and can be used as is.
* jx-node.js excludes dependencies that could be installed via npm install.
* Both using UMD pattern, so they should work fine with different modules.

### To use library just add it on the page/require it in node and init instance of library ###

Web:

```
#!html


<script src="../jx-web.js"></script>
<script>
   var jx = new JX();
</script>

```

Node:


```
#!javascript

var jx = new(require('../../dist/jx-node.js'))();
```

There are two methods there 

This method is the same for client and for server.

```
#!javascript

jx.toXLSX(json_data, filename)
```

You will receive Error instance if there was any error during processing.
You can hande it like:

```
#!javascript

var result = jx.toXLSX(json_data, filename)
if(result instanceof Error) {
    // Do whatever you want.
}
```


This function have differences for node and web.
Node version requires filename and callback, but web version requires file itself (you can get it from input[type=file] as example) and callback.


```
#!javascript

jx.toJSON(file_or_filename, callback);
```


Callback receives 1 parameter;

```
#!javascript


jx.toJSON(file_or_filename, function callback(result) {
    //Error processing
    if(result instanceof Error) {
        //we've got an error
    } else {
        //everything is fine
    }
})
```