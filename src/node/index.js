import Converter from '../common/index.js';
import err from '../common/error.js';
import X from 'xlsx';
import FileAPI from 'file-api';

export default class JX {
    constructor() {
        this.converter = new Converter();
    }
    toJSON(filename, cb) {
        if (filename.trim() === '') {
            cb(err('JxNode', 'N1', 'Filename cannot be empty.'));
            return;
        }
        let reader = new FileAPI.FileReader();
        let file = new FileAPI.File(filename);
        reader.onload = (e) => {
            let readOptions = {
                "type": "binary"
            }
            let data;
            try {
                data = X.read(e.target.result, readOptions);
            } catch (e) {
                cb(err('JxNode', 'N2', 'XLSX parsing error.'));
                return;
            }
            this.converter.toJSON(data, cb);
        };
        reader.onerror = (e) => {
            cb(err('JxNode', 'N3', 'File reading error.'));
        }
        reader.readAsBinaryString(file);
    }
    toXLSX(json, filename = "file") {
        let wb = this.converter.toXLSX(json);
        if (wb instanceof Error) {
            return wb;
        }
        try {
            X.writeFile(wb, filename + ".xlsx");
        } catch (e) {
            err('JxNode', 'N4', 'File writing error.');
        }
    }
}