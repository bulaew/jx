export default function error(module, code, description) {
    let error = new Error(module);
    error.code = code;
    error.description = description;
    return error;
}