var jx = new(require('../../dist/jx-node.js'))();

//reading data from example.xlsx file
jx.toJSON('../assets/simple.xlsx', cb);

function cb(result) {
    // If there was error our result will be instance of Error
    if (result instanceof Error) {
        console.log(result);
    } else {
        /* Else we will get result in format
            result : {
                json: {
                    "SheetName" : [ rows ]
                }
                sheets : [ sheets names ]
            }
        */
        // Just printing result on the screen :)
        result.sheets.forEach(function(name) {
            result.json[name].forEach(function(obj) {
                console.log(obj);
            })
        })
    }
    // Transformation from json to XLSX and file writing.
    var xlsx = jx.toXLSX(result.json, 'result/complete');
    // We will receive Error instance if there was error.
    // Else "undefined"
    if (xlsx instanceof Error) {
        console.log("Error");
    } else {
        console.log("Everything fine");
    }
}