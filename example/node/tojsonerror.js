var jx = new(require('../../dist/jx-node.js'))();

function cb(result) {
    if (result instanceof Error) {
        console.log(result.description);
    }
}

jx.toJSON('../assets/invalid.xlsx', cb);
jx.toJSON('anything.xlsx', cb);
jx.toJSON('', cb);